package com.zxh.cloudDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/*
*@EnableDiscoveryClient,通过其向服务中心注册；
* 并且向程序的ioc注入一个bean: restTemplate;并通过
* @LoadBalanced注解表明这个restRemplate开启 负载均衡的功能。
* */
@EnableDiscoveryClient
@SpringBootApplication
@EnableHystrix
@EnableHystrixDashboard
public class ServiceRibbonHystrixDashboardApplication  {

	public static void main(String[] args)
	{
		SpringApplication.run(ServiceRibbonHystrixDashboardApplication .class, args);
	}

	@Bean
	@LoadBalanced
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
