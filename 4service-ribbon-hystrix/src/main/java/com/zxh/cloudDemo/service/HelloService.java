package com.zxh.cloudDemo.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by zxhhd on 2017/12/31.
 * HystrixCommand注解，指定fallbackMethod 熔斷方法，返回一個字符串。
 * 当SERVICE-HI不可用的时候，调用接口时，会执行快速失败，即熔断方法，直接返回一组字符串。
 * 而不是等待响应超时。控制了容器的线程阻塞。
 */
@Service
public class HelloService {

	@Autowired
	RestTemplate restTemplate;

	@HystrixCommand(fallbackMethod = "hiError")
	public String hiService(String name) {
		return restTemplate.getForObject("http://this-is-service/hi?name="+name,String.class);
	}

	public String hiError(String name) {
		return "hi,"+name+",sorry,error!";
	}
}
