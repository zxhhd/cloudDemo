package com.zxh.cloudDemo;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.netflix.turbine.EnableTurbine;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//当我们有很多个服务的时候，这就需要聚合所以服务的Hystrix Dashboard的数据了。
//这就需要用到Spring Cloud的另一个组件了，即Hystrix Turbine。
//看单个的Hystrix Dashboard的数据并没有什么多大的价值，
//要想看这个系统的Hystrix Dashboard数据就需要用到Hystrix Turbine。
//Hystrix Turbine将每个服务Hystrix Dashboard数据进行了整合。
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@RestController
@EnableHystrix
@EnableHystrixDashboard
@EnableCircuitBreaker
@EnableTurbine
public class TurbineApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TurbineApplication.class, args);
	}
}
