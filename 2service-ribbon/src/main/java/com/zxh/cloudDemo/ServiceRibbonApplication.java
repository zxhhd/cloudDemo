package com.zxh.cloudDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/*
*@EnableDiscoveryClient,通过其向服务中心注册；
* */
@EnableDiscoveryClient
@SpringBootApplication
@EnableEurekaClient
public class ServiceRibbonApplication  {

	public static void main(String[] args) {
		SpringApplication.run(ServiceRibbonApplication .class, args);
	}

	@Bean   //向程序的ioc注入一个bean restTemplate
	@LoadBalanced //表明这个RestTemplate开启负载均衡的功能
	RestTemplate restTemplate(){
		return new RestTemplate();
	}
}
