package com.zxh.cloudDemo;

import org.springframework.stereotype.Component;

/**
 * Created by zxhhd on 2017/12/31.
 */
@Component
public class SchedualServiceHiHystric implements SchedualServiceHi {
	@Override
	public String sayHiFromClientOne(String name) {
		return "sorry "+name;
	}
}
