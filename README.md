# cloudDemo
cloud
第一篇: 服务的注册与发现（Eureka）  eureka-client
第二篇: 服务消费者（rest+ribbon）  eureka-client
第三篇: 服务消费者（Feign）    serice-feign
第四篇: 断路器（Hystrix）      service-ribbon-hystrix  service-feign-hystrix
第五篇: 路由网关(zuul)     service-zuul
第六篇: 分布式配置中心(Spring Cloud Config)
第七篇: 高可用的分布式配置中心(Spring Cloud Config)
第八篇: 消息总线(Spring Cloud Bus)
第九篇: 服务链路追踪(Spring Cloud Sleuth)
第十篇: 高可用的服务注册中心
第十一篇:docker部署spring cloud项目
第十二篇: 断路器监控(Hystrix Dashboard)
第十三篇: 断路器聚合监控(Hystrix Turbine)
第十四篇: 服务注册(consul)

一、
启动eureka-server作为服务注册中心
启动eureka-client作为服务提供者，注册在服务中心。其信息可在服务主页面看到。其接口可通过相应的controller访问。http访问

二、
ribbon是一个负载均衡客户端，可以很好的控制htt和tcp的一些行为。Feign默认集成了ribbon
启动eureka-server 8761
启动eureka-client 8762
再修改eureka-client的配置文件，以8763端口另起一个服务。（这时相当于两个服务的集群，且服务名相同。用于ribbon做负载均衡，有两个服务端。）
启动service-ribbon，在controller中调用service，在service中通过restTemplate调用eureka-client（this-is-service）服务(在同一个服务端注册，可以通过服务名相互调用)。
此时访问controller，会发现在 8762  8763间已经通过service-ribbon实现了负载均衡的效果。

三、
Feign是一个声明式的伪Http客户端，它使得写Http客户端变得更简单。使用Feign，只需要创建一个接口并注解。
它具有可插拔的注解特性，可使用Feign 注解和JAX-RS注解。Feign支持可插拔的编码器和解码器。Feign默认集成了Ribbon，并和Eureka结合，默认实现了负载均衡的效果。
和ribbon一样，起两个eureka-client，同样是为了实现负载均衡。Feign包含了ribbon，实现方式不同
启动service-feign

四、
启动service-ribbon-hystrix，其controller调用service，再通过ribbon调用service-hi。熔断器的功能也就是
为了在服务相互调用的时候，若被调用的服务出现故障，通过@HystrixCommand(fallbackMethod = "hiError")
该注解，提供一个可以返回指定默认结果的方法。

启动service-feign-hystrix
其作用和 四  一样，只是借助feign实现。

五、路由网关(zuul)
Zuul的主要功能是路由转发和过滤器
运行service-zuul
依次运行这五个工程;打开浏览器访问：http://localhost:8769/api-a/hi?name=forezp
加过过滤器后，访问：http://localhost:8769/api-a/hi?name=forezp&token=22。就不会被拦截了。

六、
启动config-server：访问http://localhost:8888/foo/dev   如可显示，证明配置服务中心可以从远程程序获取配置信息。
启动config-client，打开网址访问：http://localhost:8881/hi   如显示：foo version 3
这就说明，config-client从config-server获取了foo的属性，而config-server是从git仓库读取的：

七、配置中心集群，为了高可用
启动eureka-server-config，。

改造config-server为如下配置
spring.application.name=config-server
server.port=8888
spring.cloud.config.server.git.uri=https://github.com/forezp/SpringcloudConfig/
spring.cloud.config.server.git.searchPaths=respo
spring.cloud.config.label=master
spring.cloud.config.server.git.username= your username
spring.cloud.config.server.git.password= your password
eureka.client.serviceUrl.defaultZone=http://localhost:8889/eureka/

改造config-client
配置文件bootstrap.properties，加上服务注册地址为http://localhost:8889/eureka/
spring.cloud.config.discovery.enabled 是从配置中心读取文件。
spring.cloud.config.discovery.serviceId 配置中心的servieId，即服务名。
这时发现，在读取配置文件不再写ip地址，而是服务名，这时如果配置服务部署多份，通过负载均衡，从而高可用。

依次启动eureka-servr,config-server,config-client
访问网址：http://localhost:8889/

八、Bus(消息总线)
Bus 将分布式的节点用轻量的消息代理连接起来。它可以用于广播配置文件的更改或者服务之间的通讯，也可以用于监控。





















