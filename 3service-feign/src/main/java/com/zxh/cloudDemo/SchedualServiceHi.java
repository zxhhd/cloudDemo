package com.zxh.cloudDemo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by zxhhd on 2017/12/31.
 * 在此interface中，关键是 @FeignClient(value = "service-hi")，
 * 指定了通過Feign將此接口映射到 service-hi 該client中去。
 */
@FeignClient(value = "this-is-service")
public interface SchedualServiceHi {

	@RequestMapping(value = "/hi",method = RequestMethod.GET)
	String sayHiFromClientOne(@RequestParam(value = "name") String name);

}
